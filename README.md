# Reusable App
This is a Django example for creating a a reusable app.

## Installing some prerequisites
I am going to use setuptools to build my
package. It’s the recommended packaging tool (merged with the 
distribute fork). I’ll also be using pip to install and uninstall 
it.

#### 1. Change directories
Move the polls directory into the django-polls directory.

#### 2. README.rst
Create a file django-polls/README.rst

#### 3. LICENSE file
Choosing a license is beyond the scope of this project, but
suffice it to say that code released publicly without a license is
useless. Django and many Django-compatible apps are distributed 
under the BSD license; however, you’re free to pick your own 
license. Just be aware that your licensing choice will affect 
who is able to use your code.

#### 4. setup.cfg and setup.py
Next we’ll create setup.cfg and setup.py files which detail how to 
build and install the app. The setuptools documentation has a good
explanation.

#### 5. MANIFEST.in
Only Python modules and packages are included in the package by 
default. To include additional files, we’ll need to create a 
MANIFEST.in file. The setuptools docs referred to in the previous 
step discuss this file in more details. It’s optional, but
recommended, to include detailed documentation with your app. 

#### 6. Building your package
Try building your package with python setup.py sdist 
(run from inside django-polls). This creates a directory called 
dist and builds your new package, django-polls-1.0.0.tar.gz.


# Acknowledgements
**DjangoProject** © 2020+. Released under the [Django] License.<br>


>[Django]: https://docs.djangoproject.com/
