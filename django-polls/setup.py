from setuptools import setup, find_packages

setup(name='django-polls',
      version='1.0.0',
      packages=find_packages())